from django.urls import path
from . import views

app_name = "accounts"

urlpatterns = [
        path('signin/', views.signin, name='signin'),
        path('signup/', views.signup, name='signup'),
        path('profile/<str:user_name>', views.profile, name='profile'),
        path('edit/', views.edit, name='edit'),
        path('logout/', views.out, name='out'),
        path('rank/<str:token>', views.rank, name='rank')
]
