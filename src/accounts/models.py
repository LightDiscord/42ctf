from django.db import models
from django.contrib.auth import models as auth_models
from django.contrib.auth.models import timezone
from django.db.models import F, Min, Max
from secrets import token_hex

# Create your models here
class UserProfileInfo(models.Model):
    user                    =   models.OneToOneField(auth_models.User, on_delete=models.CASCADE)
    portfolio_site          =   models.URLField(blank=True)
    score                   =   models.PositiveIntegerField(default=0, db_index=True)
    last_submission_date    =   models.DateTimeField('Last Submission Date', default=timezone.now)
    token                   =   models.CharField(max_length=200, blank=True)
    def __str__(self):
        return self.user.username
    class Meta:
        ordering = ['-score', 'last_submission_date', 'user__username']
        verbose_name = 'profile'
        verbose_name_plural = 'profiles'

# Create your models here.
