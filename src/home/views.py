from django.shortcuts import render
from django.http import HttpResponse
from .models import new
from ctfs.models import Category, CTF, CTF_flags
from accounts.models import UserProfileInfo

def home(request):
    news        =   new.objects.order_by('-pub_date')[:5]
    latest_ctfs =   CTF.objects.order_by('-pub_date')[:5]
    top10       =   UserProfileInfo.objects.select_related().order_by('-score', 'last_submission_date', 'user__username')[:10]
    nb_flags    =   CTF_flags.objects.count()
    nb_users    =   UserProfileInfo.objects.count()
    return render(request, 'home/home.html', {'news' : news, 'ctfs' : latest_ctfs, 'top' : top10, 'flags' : nb_flags})

# Create your views here.
